@extends('adminlte::page')



@section('title', 'Dashboard')



@section('content_header')

<h1>Buat Post</h1>

@stop



@section('content')
<div class="box box-solid">
    <div class="box-header with-border" style="background-color:#34495e;color:white;">
        <h3 class="box-title"><b>Create</b> Post</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <form>
            <div class="col-md-6">
                <label>Title:</label>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
                <label>Summary:</label>
                <div class="form-group" style="width:100%">
                    <div class="input-group" style="width:100%">
                        <textarea type="text" class="form-control" rows="5" style="width:100%;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"
                            readonly></textarea>
                    </div>
                </div>
                <label>Description:</label>
                <div class="form-group" style="width:100%">
                    <div class="input-group" style="width:100%">
                        <textarea type="text" class="tinymce" rows="5" style="width:100%;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"
                            readonly></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label>Keyword:</label>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
                <label>Publish Date:</label>
                <div class="form-group" style="width:100%">
                    <div class="input-group" style="width:100%">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
                <label>Title:</label>
                <div class="form-group" style="width:100%">
                    <div class="input-group" style="width:100%">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
                <label>Template:</label>
                <div class="form-group" style="width:100%">
                    <div class="input-group" style="width:100%">
                        <select class="form-control" readonly>
                            <option value="default">Default</option>
                        </select>
                    </div>
                </div>
                <label>Select Image:</label>
                <div class="form-group" style="width:100%">
                    <div class="input-group" style="width:100%">
                        <button class="btn btn-primary">Browse</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@stop



@section('css')

<link rel="stylesheet" href="/css/admin_custom.css">
<style>
    .employe-form td {
        padding-right: 24px;
    }

</style>
@stop


@section('js')
    <script type="text/javascript" src="{{ URL::asset('js')}}/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="{{ URL::asset('js')}}/tinymce/init-tinymce.js"></script>
@stop
