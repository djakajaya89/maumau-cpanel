@extends('adminlte::page')



@section('title', 'Dashboard')



@section('content_header')

<h1>Dashboard</h1>

@stop



@section('content')
<div class="box box-solid box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">List Pegawai</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-striped table-bordered table-hover data-table" id="sample_2">
            </caption>
            <thead style="background-color:#f2f2f2">
                <tr>
                    <th>
                        NO
                    </th>
                    <th>
                        Fullname
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Phone No
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Joined Date
                    </th>
                    <th>
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @for ($i = 0; $i < count($staff); $i++) <td>{{$staff[$i]->id}}</td>
                        <td>{{$staff[$i]->name}}</td>
                        <td>{{$staff[$i]->email}}</td>
                        <td>{{$staff[$i]->phone}}</td>
                        <td>{{$staff[$i]->status}}</td>
                        <td>{{$staff[$i]->join_date}}</td>
                        <td><a href="{{url('/')}}/admin/staff/edit/{{$staff[$i]->id}}" data-skin="skin-yellow-light"
                                class="btn btn-warning btn-xs"><i class="fa fa-eye"></i></a></td>
                        @endfor
                </tr>
            </tbody>
        </table>
    </div>
</div>
@stop



@section('css')

<link rel="stylesheet" href="/css/admin_custom.css">

@stop


@section('js')
<script>
    $(document).ready(function () {
        $('.data-table').dataTable();
    });

</script>
@stop
