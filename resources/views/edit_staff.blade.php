@extends('adminlte::page')



@section('title', 'Dashboard')



@section('content_header')

<h1>Edit Staff</h1>

@stop



@section('content')
<div class="box box-solid box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Employe Information</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="box-picture col-md-5" style="text-align:center;padding-top:64px;">
            <img width="128px" src="{{ URL::asset('svg')}}/avatar.svg"/>
            <h3>Avatar</h3>
        </div>
        <div class="col-md-7 employe-form">
            <table style="width:100%;text-align:right">
                <tr>
                    <td>
                        <label>Full Name</label>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" class="form-control" value="{{$staff->name}}" readonly>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Email</label>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <input type="text" class="form-control" value="{{$staff->email}}" readonly>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Phone no</label>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" class="form-control" value="{{$staff->phone}}" readonly>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Date of Birth</label>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" value="{{$staff->birthdate}}" readonly>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Description</label>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-pencil"></i>
                                </div>
                                <textarea type="text" class="form-control" readonly>{{$staff->description}}</textarea>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>File Assasement</label>
                    </td>
                    <td>
                        <div class="form-group form-inline" style="text-align:left">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-file"></i>
                                </div>
                                <input type="file" class="form-control" readonly>
                            </div>
                            <button class="btn btn-success">Upload</button>
                        </div>
                    </td>
                </tr>
                <tr style="text-align:left">
                    <td></td>
                    <td>
                        <a href="{{url('/')}}/admin/staff/{{$staff->id}}/assign"><button class="btn btn-primary">Assign Job</button></a>
                        <button class="btn btn-primary">Download Resume</button>
                        <button class="btn btn-primary">Download FIle Assasement</button>
                    </td>
                <tr>
            </table>
        </div>
    </div>
</div>

<div class="box box-solid box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Work Experience</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="col-md-12" style="text-align:left;border:black solid 2px;padding:24px;font-weight:bold">
            <table class="col-md-12">
                @foreach ($exp as $i)
                <tr>
                        <td class="col-md-2">
                            Position
                        </td>
                        <td class="col-md-1">
                            &nbsp;:&nbsp;
                        </td>
                        <td class="col-md-7">
                            {{$i->position}}
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">
                            &nbsp;&nbsp;
                        </td>
                        <td class="col-md-1">
                            &nbsp;&nbsp;
                        </td>
                        <td class="col-md-7">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">
                            Company
                        </td>
                        <td class="col-md-1">
                            &nbsp;:&nbsp;
                        </td>
                        <td class="col-md-7">
                            {{$i->company}}
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">
                            &nbsp;&nbsp;
                        </td>
                        <td class="col-md-1">
                            &nbsp;&nbsp;
                        </td>
                        <td class="col-md-7">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">
                            From
                        </td>
                        <td class="col-md-1">
                            &nbsp;:&nbsp;
                        </td>
                        <td class="col-md-7">
                            {{$i->duration}}
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2">
                            &nbsp;&nbsp;
                        </td>
                        <td class="col-md-1">
                            &nbsp;&nbsp;
                        </td>
                        <td class="col-md-7">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

@stop



@section('css')

<link rel="stylesheet" href="/css/admin_custom.css">
<style>
    .employe-form td {
        padding-right: 24px;
    }

</style>
@stop


@section('js')
<script>
    $('#my-box').boxWidget({
        animationSpeed: 500,
        collapseTrigger: '#my-collapse-button-trigger',
        removeTrigger: '#my-remove-button-trigger',
        collapseIcon: 'fa-minus',
        expandIcon: 'fa-plus',
        removeIcon: 'fa-times'
    })

</script>
@stop
