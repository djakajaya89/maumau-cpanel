-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Okt 2018 pada 14.44
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maumau`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_employe`
--

CREATE TABLE `t_employe` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `status` varchar(10) NOT NULL,
  `join_date` date NOT NULL,
  `birthdate` date NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_employe`
--

INSERT INTO `t_employe` (`id`, `name`, `email`, `phone`, `status`, `join_date`, `birthdate`, `description`) VALUES
(1, 'Djaka Pradana Jaya Priambudi', 'djakajaya89@gmail.com', '082137748547', 'NEW', '2018-10-01', '2018-11-05', 'None');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_experience`
--

CREATE TABLE `t_experience` (
  `id` int(11) NOT NULL,
  `employe_id` int(11) NOT NULL,
  `position` varchar(32) NOT NULL,
  `duration` varchar(32) NOT NULL,
  `company` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_experience`
--

INSERT INTO `t_experience` (`id`, `employe_id`, `position`, `duration`, `company`) VALUES
(1, 1, 'Programmer', '5 November 2018 - 14 Februari 20', 'PT Telkom Indonesia');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `t_employe`
--
ALTER TABLE `t_employe`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `t_experience`
--
ALTER TABLE `t_experience`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `t_employe`
--
ALTER TABLE `t_employe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `t_experience`
--
ALTER TABLE `t_experience`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
