<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Staff extends Model
{
    public static function GetData(){
        $value=DB::table('t_employe')->get();
        return $value;
    }

    public static function GetById($id){
        $value=DB::table('t_employe')->where('id', $id)->first();
        return $value;
    }

    public static function GetExperience($id){
        $value=DB::table('t_experience')->where('employe_id', $id)->get();
        return $value;
    }
}
