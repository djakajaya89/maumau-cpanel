<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Staff;

class Admin extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }

    public function admin(){
        $staff = Staff::GetData();
        return view('admin', ["staff"=>$staff]);
    }

    public function staff_edit(Request $request,$id){
        $staff = Staff::GetById($id);
        $experience = Staff::GetExperience($id);
        return view('edit_staff', ["staff"=>$staff, "exp"=>$experience]);
    }

    public function assign_job(Request $resquest, $id){
        return view('assign_job');
    }
}